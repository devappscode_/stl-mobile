import {
  LOGIN,
  REGISTER,
  ACCOUNTDETAILS,
  LOADL2,
  LOADS3,
  LOADHISTORY,
} from './types';

export const login = (payload) => ({
  type: LOGIN,
  payload: payload,
});

export const register = (data) => ({
  type: REGISTER,
  data: data,
});

export const loadL2 = (data) => ({
  type: LOADL2,
  data: data,
});

export const loadS3 = (data) => ({
  type: LOADS3,
  data: data,
});

export const loadHistory = (data) => ({
  type: LOADHISTORY,
  data: data,
});

export const accountDetails = (data) => ({
  type: ACCOUNTDETAILS,
  data: data,
});
