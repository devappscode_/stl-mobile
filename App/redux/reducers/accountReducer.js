import {
  REGISTER,
  LOGIN,
  LOADS3,
  LOADL2,
  ACCOUNTDETAILS,
  LOADHISTORY,
} from '../actions/types';

const initialState = {
  account: {},
  accountDetails: {},
  isLogin: false,
  isRegistered: false,
  l2: {},
  s3: {},
  history: {},
};

const accountReducer = (state = initialState, action) => {
  let actionType = action.type;
  switch (actionType) {
    case LOGIN:
      return {
        ...state,
        account: action.payload,
        isLogin: true,
      };

    case REGISTER:
      return {
        ...state,
        account: action.data,
      };
    case LOADL2:
      console.log('loading L2');
      return {
        ...state,
        l2: action.data,
      };
    case LOADS3:
      return {
        ...state,
        s3: action.data,
      };
    case LOADHISTORY:
      return {
        ...state,
        history: action.data,
      };

    case ACCOUNTDETAILS:
      return {
        ...state,
        accountDetails: action.data,
      };

    default:
      return state;
  }
};

export default accountReducer;
