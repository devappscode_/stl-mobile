import React, {Component} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  ImageBackground,
  Button,
  ScrollView,
} from 'react-native';

//redux
import {connect} from 'react-redux';
import {login, loadHistory} from '../../redux/actions/account';

//api
import {POST, GET} from '../../api/service/service';
import {URL} from '../../constants/apirUrls';
import HistoryItem from '../../components/HistoryItem';
import {initData, modifyData, resetData} from '../../Helpers/helpers';

import {DEVICE_HEIGHT, DEVICE_WIDTH} from '../../constants/constants';

class Data extends Component {
  constructor(props) {
    super(props);
    this.state = this._getState();
  }

  _getState = () => ({
    email: 'user1@gmail.com',
    password: 'qqww1122',
  });

  componentDidMount() {
    this._isMounted = true;
    if (this._isMounted) {
      const callback = (data) => {
        const parsedData = JSON.parse(data);
        console.log(parsedData);
        this.props.loadHistory(parsedData);
        console.log('here');
        console.log(this.props.history);
        this.setState({
          loading: false,
        });
      };
      const config = {
        key: 'history',
        callback,
      };
      initData(config);
    }
  }

  render() {
    const {history} = this.props;
    return (
      <View style={styles.mainContainer}>
        <View style={styles.optionsView}>
          <View style={styles.fillerContainer} />
          <Text style={styles.titleText}>History</Text>
          <View style={styles.fillerContainer} />
        </View>
        <View style={styles.listContainer}>
          <ScrollView>
            <HistoryItem data={history} />
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#effcef',
  },
  optionsView: {
    height: DEVICE_HEIGHT * 0.07,
    width: DEVICE_WIDTH * 1,
    backgroundColor: '#655c56',
    paddingHorizontal: DEVICE_WIDTH * 0.025,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: DEVICE_HEIGHT * 0.01,
  },
  titleText: {
    fontSize: DEVICE_HEIGHT * 0.03,
    color: '#FFFFFF',
    fontFamily: 'Alata-Regular',
  },
  fillerContainer: {
    width: DEVICE_WIDTH * 0.07,
  },
});

const mapStateToProps = (state) => {
  return {
    account: state.accountReducer.account,
    isLogin: state.accountReducer.isLogin,
    accountDetails: state.accountReducer.accountDetails,
    l2: state.accountReducer.l2,
    s3: state.accountReducer.s3,
    history: state.accountReducer.history,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    login: (payload) => dispatch(login(payload)),
    loadL2: (payload) => dispatch(loadL2(payload)),
    loadS3: (payload) => dispatch(loadS3(payload)),
    loadHistory: (payload) => dispatch(loadHistory(payload)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Data);
