import React, {Component} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  ImageBackground,
  Button,
  FlatList,
  Image,
  TextInput,
  ActivityIndicator,
  ScrollView,
} from 'react-native';

//redux
import {connect} from 'react-redux';
import {login, loadS3, loadL2} from '../../../redux/actions/account';

//helpers
import {initData, modifyData, resetData} from '../../../Helpers/helpers';

//storage
import BetItem from '../../../components/betItem';
import {DEVICE_HEIGHT, DEVICE_WIDTH} from '../../../constants/constants';
import Modal, {ModalContent} from 'react-native-modal';

class Lastwo extends Component {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = this._getState();
  }

  _getState = () => ({
    data: {},
    loading: true,
    modalVisible: false,
    inputDigit: null,
    inputAmount: null,
  });

  componentDidMount() {
    this._isMounted = true;
    if (this._isMounted) {
      const callback = (data) => {
        const parsedData = JSON.parse(data);
        this.props.loadL2(parsedData);
        console.log('here');
        console.log(this.props.l2);
        this.setState({
          loading: false,
        });
      };
      const config = {
        key: 'l2',
        callback,
      };
      initData(config);
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  onPressAdd = () => {
    const {inputAmount, inputDigit} = this.state;
    const {l2} = this.props;
    l2[inputDigit] = inputAmount;
    this.setState({
      inputAmount: null,
      inputDigit: null,
      modalVisible: false,
    });
    const config = {
      data: JSON.stringify(l2),
      key: 'l2',
    };
    modifyData(config);
    console.log(
      'added digit: ' + inputDigit + ' with amount of ' + inputAmount,
    );
  };

  onPressSave = () => {
    let {inputDigit, inputAmount} = this.state;
    inputDigit.length >= 2 && inputAmount
      ? this.onPressAdd()
      : alert('Insufficient Input');
  };

  onPressClose = () => {
    this.toggleModal();
  };

  toggleModal = () => {
    this.setState({
      modalVisible: !this.state.modalVisible,
    });
  };

  onChangeDigit = (value) => {
    let {l2} = this.props;
    this.setState({
      inputDigit: value,
    });

    if (value.length > 1) {
      const isExisting = l2[value] ? true : false;
      if (isExisting) {
        let inputAmount = l2[value].toString();
        this.setState({
          inputAmount,
        });
      }
    } else {
      this.setState({
        inputAmount: null,
      });
    }
  };

  onChangeAmount = (value) => {
    let amount = this.toMoney(value);
    this.setState({
      inputAmount: amount,
    });
  };

  toMoney = (amount) => {
    return amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  };

  onPressSubmit = () => {
    const {push} = this.props.navigation;
    const type = 'l2';
    push('Transact', {type});
  };

  renderModifyModal = () => {
    let {inputDigit, inputAmount} = this.state;
    return (
      <View style={styles.modifyView}>
        <TouchableOpacity
          style={styles.closeButton}
          onPress={() => this.onPressClose()}>
          <Image
            style={styles.closeIcon}
            source={require('../../../assets/images/close.png')}
          />
        </TouchableOpacity>
        <View style={{flexDirection: 'row', width: DEVICE_WIDTH * 0.7}}>
          <View style={styles.modifySubView}>
            <TextInput
              style={styles.input}
              keyboardType={'number-pad'}
              maxLength={2}
              value={inputDigit}
              onChangeText={(text) => this.onChangeDigit(text)}
              ref={(input) => {
                this.textInput = input;
              }}
            />
            <Text style={styles.inputLabelText}>Digit</Text>
          </View>
          <View style={styles.modifySubView}>
            <TextInput
              style={[styles.input, {width: DEVICE_WIDTH * 0.3}]}
              keyboardType={'number-pad'}
              value={inputAmount}
              onChangeText={(text) => this.onChangeAmount(text)}
              ref={(input) => {
                this.textInput = input;
              }}
            />
            <Text style={styles.inputLabelText}>Amount</Text>
          </View>
        </View>
        <TouchableOpacity
          style={styles.modifyButton}
          onPress={() => this.onPressSave()}>
          <Image
            style={styles.modifyIcon}
            source={require('../../../assets/images/edit.png')}
          />
          <Text style={styles.modifyButtonText}>Save</Text>
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    let {data, loading, modalVisible} = this.state;
    const {l2} = this.props;
    return (
      <View style={styles.mainContainer}>
        <View style={styles.optionsView}>
          <View style={styles.fillerContainer} />
          <Text style={styles.titleText}>Lastwo</Text>
          <TouchableOpacity onPress={() => this.toggleModal()}>
            <Image
              style={styles.addIcon}
              source={require('../../../assets/images/add.png')}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.typeContainer}>
          <Text style={styles.typeText}>Digit</Text>
          <Text style={styles.typeText}>Amount</Text>
        </View>
        <View style={styles.listContainer}>
          <ScrollView>
            {!loading ? (
              <BetItem data={l2} />
            ) : (
              <ActivityIndicator size="large" color="#FFBA00" />
            )}
          </ScrollView>
        </View>
        {!loading && (
          <Modal
            isVisible={modalVisible}
            onBackdropPress={() => this.onPressClose()}
            hideModalContentWhileAnimating={true}>
            {this.renderModifyModal()}
          </Modal>
        )}
        <TouchableOpacity
          style={styles.submitButton}
          onPress={() => this.onPressSubmit()}>
          <Text style={styles.submitText}>Submit</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#effcef',
  },
  titleText: {
    fontSize: DEVICE_HEIGHT * 0.03,
    color: '#FFFFFF',
    fontFamily: 'Alata-Regular',
  },
  modifyView: {
    height: DEVICE_HEIGHT * 0.3,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#effcef',
    borderRadius: DEVICE_HEIGHT * 0.03,
  },
  modifyButton: {
    height: DEVICE_HEIGHT * 0.07,
    width: DEVICE_WIDTH * 0.6,
    borderRadius: DEVICE_HEIGHT * 0.1,
    backgroundColor: '#655c56',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: DEVICE_HEIGHT * 0.05,
  },
  modifyButtonText: {
    fontSize: DEVICE_HEIGHT * 0.025,
    color: '#FFFFFF',
    fontFamily: 'Alata-Regular',
    fontWeight: 'bold',
  },
  submitButton: {
    height: DEVICE_HEIGHT * 0.07,
    width: DEVICE_WIDTH * 0.6,
    borderRadius: DEVICE_HEIGHT * 0.1,
    backgroundColor: '#655c56',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: DEVICE_HEIGHT * 0.05,
    position: 'absolute',
    bottom: DEVICE_HEIGHT * 0.005,
  },
  submitText: {
    fontSize: DEVICE_HEIGHT * 0.02,
    color: '#FFFFFF',
    fontFamily: 'Alata-Regular',
    fontWeight: 'bold',
  },
  modifyIcon: {
    tintColor: '#FFFFFF',
    width: DEVICE_WIDTH * 0.05,
    resizeMode: 'contain',
    marginRight: DEVICE_WIDTH * 0.03,
  },
  typeContainer: {
    flexDirection: 'row',
    height: DEVICE_HEIGHT * 0.06,
    marginTop: DEVICE_HEIGHT * 0.01,
    width: DEVICE_WIDTH * 0.95,
    borderTopLeftRadius: DEVICE_HEIGHT * 0.03,
    borderTopRightRadius: DEVICE_HEIGHT * 0.03,
    justifyContent: 'space-around',
    backgroundColor: '#94d3ac',
    alignItems: 'center',
    borderBottomColor: '#F1F1F1',
    borderBottomWidth: 1,
  },
  listContainer: {
    maxHeight: DEVICE_HEIGHT * 0.68,
    width: DEVICE_WIDTH * 0.95,
    borderBottomLeftRadius: DEVICE_HEIGHT * 0.03,
    borderBottomRightRadius: DEVICE_HEIGHT * 0.03,
    backgroundColor: '#ccedd2',
    paddingVertical: DEVICE_HEIGHT * 0.02,
  },
  optionsView: {
    height: DEVICE_HEIGHT * 0.07,
    width: DEVICE_WIDTH * 1,
    backgroundColor: '#655c56',
    paddingHorizontal: DEVICE_WIDTH * 0.025,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  typeText: {
    fontSize: DEVICE_HEIGHT * 0.03,
    color: '#0C3B2E',
    fontFamily: 'Alata-Regular',
    fontWeight: 'bold',
  },

  addIcon: {
    tintColor: '#FFFFFF',
    width: DEVICE_WIDTH * 0.07,
    resizeMode: 'contain',
  },

  closeIcon: {
    tintColor: '#655c56',
    width: DEVICE_WIDTH * 0.07,
    resizeMode: 'contain',
  },

  input: {
    width: DEVICE_WIDTH * 0.1,
    borderBottomWidth: 1,
    borderBottomColor: '#382933',
    fontSize: DEVICE_HEIGHT * 0.05,
    height: DEVICE_HEIGHT * 0.07,
    padding: 0,
    textAlign: 'center',
  },

  inputLabelText: {
    fontSize: DEVICE_HEIGHT * 0.03,
    color: '#0C3B2E',
    fontFamily: 'Alata-Regular',
    fontWeight: 'bold',
  },

  modifySubView: {
    alignItems: 'center',
    justifyContent: 'center',
    height: DEVICE_HEIGHT * 0.1,
    width: DEVICE_WIDTH * 0.3,
  },

  closeButton: {
    position: 'absolute',
    right: DEVICE_WIDTH * -0.01,
    top: DEVICE_HEIGHT * -0.01,
    backgroundColor: '#FFFFFF',
    borderRadius: DEVICE_HEIGHT * 0.1,
    width: DEVICE_WIDTH * 0.05,
    height: DEVICE_WIDTH * 0.05,
    alignItems: 'center',
    justifyContent: 'center',
  },
  fillerContainer: {
    width: DEVICE_WIDTH * 0.07,
  },
});

const mapStateToProps = (state) => {
  return {
    account: state.accountReducer.account,
    isLogin: state.accountReducer.isLogin,
    l2: state.accountReducer.l2,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    login: (payload) => dispatch(login(payload)),
    loadL2: (payload) => dispatch(loadL2(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Lastwo);
