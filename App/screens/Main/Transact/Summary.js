import React, {Component} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  ImageBackground,
  Image,
  TextInput,
} from 'react-native';
import {DEVICE_HEIGHT, DEVICE_WIDTH} from '../../../constants/constants';
import {store, retrieve} from '../../../storage';
import BetItem from '../../../components/betItem';
import {initData, modifyData, resetData} from '../../../Helpers/helpers';

//redux
import {connect} from 'react-redux';
import {
  login,
  loadS3,
  loadL2,
  loadHistory,
} from '../../../redux/actions/account';

//api
import {POST, GET} from '../../../api/service/service';
import {URL} from '../../../constants/apirUrls';
import {ScrollView} from 'react-native-gesture-handler';
import {acc} from 'react-native-reanimated';
import Modal, {ModalContent} from 'react-native-modal';

class Summary extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);
    this.state = this._getState(props);
  }

  _getState = (props) => ({
    s3: {},
    l2: {},
    verifyModalVisible: false,
    successModalVisible: false,
    timeModalVisible: false,
    name: 'dave butardo',
    address: 'iwha toril',
    time: 'select time',
    loading: false,
    type: props.route.params.type,
  });

  componentDidMount() {
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  onPressSelectTime = () => {
    this.setState({
      timeModalVisible: true,
    });
  };

  onPressSubmit = () => {
    const {time} = this.state;
    time == 'select time'
      ? alert('Please Select Time')
      : this.setState({
          verifyModalVisible: true,
        });
  };
  onPressTime = (value) => {
    console.log(value);
    this.setState({
      time: value,
      timeModalVisible: false,
    });
  };
  onPressClose = () => {
    this.setState({
      timeModalVisible: false,
    });
  };

  onPressCancel = () => {
    this.setState({
      verifyModalVisible: false,
    });
  };

  resetData = () => {
    const {type} = this.state;
    if (type == 'l2') {
      this.props.loadL2({});
      const config = {
        data: '{}',
        key: 'l2',
      };
      modifyData(config);
    } else {
      this.props.loadS3({});
      const config = {
        data: '{}',
        key: 's3',
      };
      modifyData(config);
    }
  };

  saveHistory = () => {
    const {l2, s3} = this.props;
    const {type} = this.state;
    const useData = type == 'l2' ? l2 : s3;
    const callback = (data) => {
      const dateTime = this.getDateTime();
      let parsedData = JSON.parse(data);
      parsedData[dateTime] = useData;
      console.log('hitsory');
      console.log(parsedData);
      const config = {
        data: JSON.stringify(parsedData),
        key: 'history',
      };
      this.props.loadHistory(parsedData);
      modifyData(config);
      this.setState({
        loading: false,
      });
      console.log('props');
      console.log(this.props.history);
    };
    const config = {
      key: 'history',
      callback,
    };
    initData(config);
  };

  getDateTime = () => {
    var date = new Date().getDate(); //Current Date
    var month = new Date().getMonth() + 1; //Current Month
    var year = new Date().getFullYear(); //Current Year
    var hours = new Date().getHours(); //Current Hours
    var min = new Date().getMinutes(); //Current Minutes
    var sec = new Date().getSeconds(); //Current Seconds

    const dateTime =
      date + '/' + month + '/' + year + ' ' + hours + ':' + min + ':' + sec;
    return dateTime;
  };

  onPressProceed = () => {
    let {name, address, date, time, type} = this.state;
    const url = URL.SUBMIT;
    let {user_id, authToken} = this.props.account;
    const {l2, s3} = this.props;
    const useData = type == 'l2' ? l2 : s3;
    let {navigate} = this.props.navigation;
    let data = {useData, name, address, date, time, user_id, type};
    const receiver = (response) => {
      const {status} = response;
      if (status) {
        this.saveHistory();
        this.resetData();
        this.setState({
          verifyModalVisible: false,
          successModalVisible: true,
        });
      } else {
        alert('Data not sent');
      }
    };

    let payload = {
      data,
      url,
      receiver,
      authToken,
    };
    POST(payload);
  };

  onSubmitL2 = () => {};

  onPressOkay = () => {
    const {navigate} = this.props.navigation;
    const {type} = this.state;
    this.setState({
      successModalVisible: false,
    });
    if (type === 'l2') {
      navigate('Lastwo');
    } else {
      navigate('Suertres');
    }
  };

  renderTimeModal = () => {
    return (
      <View style={styles.timeContainer}>
        <Text style={styles.modalText}>Select Draw Time</Text>
        <View style={styles.timeRowContainer}>
          <TouchableOpacity
            style={styles.timeButton}
            onPress={() => this.onPressTime('M')}>
            <Text style={[styles.modalText, {color: '#FFFFFF'}]}>M</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.timeButton}
            onPress={() => this.onPressTime('D')}>
            <Text style={[styles.modalText, {color: '#FFFFFF'}]}>D</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.timeButton}
            onPress={() => this.onPressTime('MD')}>
            <Text style={[styles.modalText, {color: '#FFFFFF'}]}>MD</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  renderVerifyModal = () => {
    return (
      <View style={styles.verifyContainer}>
        <Text style={styles.modalText}>Are you sure you want to submit?</Text>
        <View style={styles.verifyRowContainer}>
          <TouchableOpacity onPress={() => this.onPressProceed()}>
            <Image
              style={styles.verifyIcon}
              source={require('../../../assets/images/check.png')}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.onPressCancel()}>
            <Image
              style={styles.verifyIcon}
              source={require('../../../assets/images/close.png')}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  renderSuccessModal = () => {
    return (
      <View style={styles.verifyContainer}>
        <Text style={styles.modalText}>Submitted</Text>
        <TouchableOpacity
          style={styles.button}
          onPress={() => this.onPressOkay()}>
          <Text style={[styles.modalText, {color: '#FFFFFF'}]}>Okay</Text>
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    const {
      timeModalVisible,
      verifyModalVisible,
      successModalVisible,
      name,
      address,
      time,
      loading,
      type,
    } = this.state;
    const {l2, s3} = this.props;
    const useData = type == 'l2' ? l2 : s3;

    return (
      <View style={styles.mainContainer}>
        <View style={styles.optionsView}>
          <TouchableOpacity onPress={() => this.toggleModal()}>
            <Image
              style={styles.backIcon}
              source={require('../../../assets/images/back.png')}
            />
          </TouchableOpacity>
          <Text style={styles.titleText}>Summary</Text>
          <View style={styles.fillerContainer} />
        </View>

        <View style={styles.listContainer}>
          <ScrollView>
            <View style={styles.typeContainer}>
              <Text style={styles.typeText}>Account Details</Text>
            </View>
            <View style={styles.infoContainer}>
              <Image
                style={styles.accountIcon}
                source={require('../../../assets/images/name.png')}
              />
              <Text style={styles.input}>{name}</Text>
            </View>
            <View style={styles.infoContainer}>
              <Image
                style={styles.addressIcon}
                source={require('../../../assets/images/address.png')}
              />
              <Text style={styles.input}>{address}</Text>
            </View>

            <View style={styles.infoContainer}>
              <Image
                style={styles.clockIcon}
                source={require('../../../assets/images/time.png')}
              />
              <TouchableOpacity
                onPress={() => this.onPressSelectTime()}
                style={styles.selectTime}>
                <Text style={styles.input}>{time}</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.typeContainer}>
              <Text style={styles.typeText}>Digit</Text>
              <Text style={styles.typeText}>Amount</Text>
            </View>
            {!loading ? (
              <BetItem data={useData} />
            ) : (
              <ActivityIndicator size="large" color="#FFBA00" />
            )}
          </ScrollView>
        </View>
        <TouchableOpacity
          style={styles.submitButton}
          onPress={() => this.onPressSubmit()}>
          <Text style={styles.submitText}>Submit</Text>
        </TouchableOpacity>
        <Modal
          isVisible={timeModalVisible}
          onBackdropPress={() => this.onPressClose()}
          hideModalContentWhileAnimating={true}>
          {this.renderTimeModal()}
        </Modal>

        <Modal
          isVisible={verifyModalVisible}
          onBackdropPress={() => this.onPressClose()}
          hideModalContentWhileAnimating={true}>
          {this.renderVerifyModal()}
        </Modal>
        <Modal
          isVisible={successModalVisible}
          onBackdropPress={() => this.onPressClose()}
          hideModalContentWhileAnimating={true}>
          {this.renderSuccessModal()}
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#effcef',
  },
  titleText: {
    fontSize: DEVICE_HEIGHT * 0.03,
    color: '#FFFFFF',
    fontFamily: 'Alata-Regular',
  },
  scrollContainer: {
    width: DEVICE_WIDTH * 0.95,
    backgroundColor: '#FFFFFF',
    borderRadius: DEVICE_HEIGHT * 0.03,
    paddingTop: DEVICE_HEIGHT * 0.02,
    paddingBottom: DEVICE_HEIGHT * 0.02,

    maxHeight: DEVICE_HEIGHT * 0.7,
  },
  spacer: {
    height: DEVICE_HEIGHT * 0.03,
  },
  optionsView: {
    height: DEVICE_HEIGHT * 0.07,
    width: DEVICE_WIDTH * 1,
    backgroundColor: '#655c56',
    paddingHorizontal: DEVICE_WIDTH * 0.025,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: DEVICE_HEIGHT * 0.01,
  },
  infoContainer: {
    marginLeft: DEVICE_WIDTH * 0.05,
    width: DEVICE_WIDTH * 0.9,
    height: DEVICE_HEIGHT * 0.07,
    borderBottomWidth: 1,
    borderColor: '#EFE4E4',
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: DEVICE_HEIGHT * 0.05,
  },
  timeInputText: {
    fontSize: DEVICE_HEIGHT * 0.03,
    fontFamily: 'Alata-Regular',
    paddingLeft: DEVICE_WIDTH * 0.04,
  },
  accountIcon: {
    tintColor: '#382933',
    width: DEVICE_WIDTH * 0.04,
    height: DEVICE_HEIGHT * 0.04,
    resizeMode: 'contain',
  },
  addressIcon: {
    tintColor: '#382933',
    width: DEVICE_WIDTH * 0.045,
    height: DEVICE_HEIGHT * 0.045,
    resizeMode: 'contain',
  },
  clockIcon: {
    tintColor: '#382933',
    width: DEVICE_WIDTH * 0.04,
    height: DEVICE_HEIGHT * 0.04,
    resizeMode: 'contain',
  },
  saveIcon: {
    tintColor: '#FFFFFF',
    width: DEVICE_WIDTH * 0.05,
    height: DEVICE_HEIGHT * 0.05,
    resizeMode: 'contain',
    marginRight: DEVICE_WIDTH * 0.02,
  },
  input: {
    flex: 1,
    fontSize: DEVICE_HEIGHT * 0.025,
    fontFamily: 'Alata-Regular',
    paddingLeft: DEVICE_WIDTH * 0.04,
  },
  button: {
    width: DEVICE_WIDTH * 0.7,
    height: DEVICE_HEIGHT * 0.07,
    borderRadius: DEVICE_HEIGHT * 0.1,
    backgroundColor: '#655c56',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: DEVICE_HEIGHT * 0.02,
  },
  fillerContainer: {
    width: DEVICE_WIDTH * 0.07,
  },
  headerContainer: {
    height: DEVICE_HEIGHT * 0.07,
    width: DEVICE_WIDTH * 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: DEVICE_HEIGHT * 0.2,
    backgroundColor: '#655c56',
  },
  headerText: {
    fontSize: DEVICE_HEIGHT * 0.03,
    color: '#FFFFFF',
    fontFamily: 'Alata-Regular',
  },
  subHeaderText: {
    fontSize: DEVICE_HEIGHT * 0.03,
    color: '#382933',
    fontFamily: 'Alata-Regular',
    alignSelf: 'center',
    marginBottom: DEVICE_HEIGHT * 0.02,
  },
  modalText: {
    fontSize: DEVICE_HEIGHT * 0.03,
    color: '#382933',
    fontFamily: 'Alata-Regular',
    alignSelf: 'center',
    marginBottom: DEVICE_HEIGHT * 0.02,
  },
  categoryText: {
    fontSize: DEVICE_HEIGHT * 0.02,
    color: '#382933',
    fontFamily: 'Alata-Regular',
    alignSelf: 'center',
    marginBottom: DEVICE_HEIGHT * 0.02,
  },
  line: {
    marginTop: DEVICE_HEIGHT * 0.01,
    borderBottomColor: '#382933',
    borderBottomWidth: 3,
    width: DEVICE_WIDTH * 0.2,
  },
  saveText: {
    fontSize: DEVICE_HEIGHT * 0.025,
    color: '#FFFFFF',
    fontFamily: 'Alata-Regular',
    fontWeight: 'bold',
  },
  listContainer: {
    maxHeight: DEVICE_HEIGHT * 0.73,
    width: DEVICE_WIDTH * 0.95,
    borderRadius: DEVICE_HEIGHT * 0.03,
    backgroundColor: '#ccedd2',
  },
  categoryContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  submitText: {
    fontSize: DEVICE_HEIGHT * 0.025,
    color: '#FFFFFF',
    fontFamily: 'Alata-Regular',
    fontWeight: 'bold',
  },
  verifyIcon: {
    tintColor: '#382933',
    width: DEVICE_WIDTH * 0.09,
    resizeMode: 'contain',
  },
  backIcon: {
    tintColor: '#FFFFFF',
    width: DEVICE_WIDTH * 0.06,
    resizeMode: 'contain',
  },

  closeButton: {
    position: 'absolute',
    right: DEVICE_WIDTH * -0.01,
    top: DEVICE_HEIGHT * -0.01,
    backgroundColor: '#effcef',
    borderRadius: DEVICE_HEIGHT * 0.1,
    width: DEVICE_WIDTH * 0.05,
    height: DEVICE_WIDTH * 0.05,
    alignItems: 'center',
    justifyContent: 'center',
  },
  verifyContainer: {
    height: DEVICE_HEIGHT * 0.3,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#effcef',
    borderRadius: DEVICE_HEIGHT * 0.03,
  },
  verifyRowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '60%',
    marginTop: DEVICE_HEIGHT * 0.04,
  },
  typeContainer: {
    flexDirection: 'row',
    height: DEVICE_HEIGHT * 0.06,
    width: DEVICE_WIDTH * 0.95,
    borderTopLeftRadius: DEVICE_HEIGHT * 0.03,
    borderTopRightRadius: DEVICE_HEIGHT * 0.03,
    justifyContent: 'space-around',
    backgroundColor: '#94d3ac',
    alignItems: 'center',
    borderBottomColor: '#F1F1F1',
    borderBottomWidth: 1,
  },
  selectTime: {
    height: DEVICE_HEIGHT * 0.04,
  },
  typeText: {
    fontSize: DEVICE_HEIGHT * 0.03,
    color: '#0C3B2E',
    fontFamily: 'Alata-Regular',
    fontWeight: 'bold',
  },
  timeContainer: {
    height: DEVICE_HEIGHT * 0.3,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#effcef',
    borderRadius: DEVICE_HEIGHT * 0.03,
  },
  submitButton: {
    height: DEVICE_HEIGHT * 0.07,
    width: DEVICE_WIDTH * 0.6,
    borderRadius: DEVICE_HEIGHT * 0.1,
    backgroundColor: '#655c56',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: DEVICE_HEIGHT * 0.05,
    position: 'absolute',
    bottom: DEVICE_HEIGHT * 0.005,
  },
  submitText: {
    fontSize: DEVICE_HEIGHT * 0.02,
    color: '#FFFFFF',
    fontFamily: 'Alata-Regular',
    fontWeight: 'bold',
  },
  timeButton: {
    width: DEVICE_WIDTH * 0.2,
    height: DEVICE_HEIGHT * 0.07,
    borderRadius: DEVICE_HEIGHT * 0.1,
    backgroundColor: '#382933',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  timeRowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '100%',
    marginTop: DEVICE_HEIGHT * 0.04,
  },
});

const mapStateToProps = (state) => {
  return {
    account: state.accountReducer.account,
    isLogin: state.accountReducer.isLogin,
    accountDetails: state.accountReducer.accountDetails,
    l2: state.accountReducer.l2,
    s3: state.accountReducer.s3,
    history: state.accountReducer.history,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    login: (payload) => dispatch(login(payload)),
    loadL2: (payload) => dispatch(loadL2(payload)),
    loadS3: (payload) => dispatch(loadS3(payload)),
    loadHistory: (payload) => dispatch(loadHistory(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Summary);
