import React, {Component} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  ImageBackground,
  Image,
  TextInput,
} from 'react-native';
import {DEVICE_HEIGHT, DEVICE_WIDTH} from '../../../constants/constants';
import {store, retrieve} from '../../../storage';

//redux
import {connect} from 'react-redux';
import {login} from '../../../redux/actions/account';

//api
import {POST, GET} from '../../../api/service/service';
import {URL} from '../../../constants/apirUrls';

class Transact extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);
    this.state = this._getState(props);
  }

  _getState = (props) => ({
    username: 'user-1',
    password: 'qqww1122',
    date: 'automatic',
    time: 'automatic',
    type: props.route.params.type,
  });

  onChangeUsername = (value) => {
    this.setState({
      username: value,
    });
  };

  onChangePassword = (value) => {
    this.setState({
      password: value,
    });
  };

  onPressLogin = () => {
    let {username, password, type} = this.state;
    let {push} = this.props.navigation;
    let data = {username, password};
    let url = URL.LOGIN;

    const receiver = (response) => {
      const status = response.status;
      if (status) {
        let authToken = response.access_token;
        let user_id = response.user_id;
        let propData = {...data, user_id, authToken};
        this.props.login(propData);
        push('Summary', {type});
      } else {
        let message = response.message;
        alert(message);
      }
    };

    let payload = {
      data,
      url,
      receiver,
    };
    POST(payload);
  };

  render() {
    let {username, password, date, time} = this.state;
    return (
      <View style={styles.mainContainer}>
        <View style={styles.headerContainer}>
          <Text style={styles.headerText}>Login</Text>
        </View>
        <View style={styles.infoContainer}>
          <Image
            style={styles.usernameIcon}
            source={require('../../../assets/images/name.png')}
          />
          <TextInput
            style={styles.input}
            onChangeText={(text) => this.onChangeUsername(text)}
            value={username}
          />
        </View>
        <View style={styles.infoContainer}>
          <Image
            style={styles.passwordIcon}
            source={require('../../../assets/images/password.png')}
          />
          <TextInput
            style={styles.input}
            onChangeText={(text) => this.onChangePassword(text)}
            value={password}
            secureTextEntry={true}
          />
        </View>
        <TouchableOpacity
          style={styles.button}
          onPress={() => this.onPressLogin()}>
          <Text style={styles.loginText}>Login</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#effcef',
  },
  infoContainer: {
    width: DEVICE_WIDTH * 0.9,
    height: DEVICE_HEIGHT * 0.07,
    borderBottomWidth: 1,
    borderColor: '#EFE4E4',
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: DEVICE_HEIGHT * 0.05,
  },

  usernameIcon: {
    tintColor: '#382933',
    width: DEVICE_WIDTH * 0.05,
    height: DEVICE_HEIGHT * 0.05,
    resizeMode: 'contain',
  },
  passwordIcon: {
    tintColor: '#382933',
    width: DEVICE_WIDTH * 0.055,
    height: DEVICE_HEIGHT * 0.055,
    resizeMode: 'contain',
  },

  input: {
    flex: 1,
    fontSize: DEVICE_HEIGHT * 0.03,
    fontFamily: 'Alata-Regular',
    paddingLeft: DEVICE_WIDTH * 0.04,
  },
  button: {
    width: DEVICE_WIDTH * 0.7,
    height: DEVICE_HEIGHT * 0.07,
    borderRadius: DEVICE_HEIGHT * 0.1,
    backgroundColor: '#655c56',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: DEVICE_HEIGHT * 0.2,
  },
  headerContainer: {
    height: DEVICE_HEIGHT * 0.07,
    width: DEVICE_WIDTH * 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: DEVICE_HEIGHT * 0.2,
    backgroundColor: '#655c56',
  },
  headerText: {
    fontSize: DEVICE_HEIGHT * 0.03,
    color: '#FFFFFF',
    fontFamily: 'Alata-Regular',
  },
  line: {
    marginTop: DEVICE_HEIGHT * 0.01,
    borderBottomColor: '#382933',
    borderBottomWidth: 3,
    width: DEVICE_WIDTH * 0.2,
  },
  loginText: {
    fontSize: DEVICE_HEIGHT * 0.025,
    color: '#FFFFFF',
    fontFamily: 'Alata-Regular',
  },
});

const mapStateToProps = (state) => {
  return {
    account: state.accountReducer.account,
    isLogin: state.accountReducer.isLogin,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    login: (payload) => dispatch(login(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Transact);
