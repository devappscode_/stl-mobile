import React, {Component} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  ImageBackground,
  Image,
  TextInput,
} from 'react-native';
import {DEVICE_HEIGHT, DEVICE_WIDTH} from '../../constants/constants';
import {store, retrieve} from '../../storage';
import Modal, {ModalContent} from 'react-native-modal';
import {accountDetails} from '../../redux/actions/account';

//redux
import {connect} from 'react-redux';
import {login} from '../../redux/actions/account';

//api
import {POST, GET} from '../../api/service/service';
import {URL} from '../../constants/apirUrls';

class Home extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);
    this.state = this._getState();
  }

  _getState = () => ({
    name: null,
    address: null,
    time: null,
    timeModalVisible: false,
  });

  componentDidMount() {
    this._isMounted = true;
    this._isMounted ? this.initData() : null;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  initData = () => {
    console.log('initializing account data...');
    //callback
    const accountCallback = (data) => {
      if (data) {
        console.log('Succeeding Launch');
        this.retrieveData(data);
      } else {
        console.log('First Launch');
      }
    };

    //check if first time
    retrieve('account', accountCallback);
  };

  retrieveData = (data) => {
    const parseData = JSON.parse(data);
    this.setState(parseData);
    let propData = {...parseData};
    this.props.accountDetails(propData);
  };

  saveData = () => {
    let {name, address, date, time} = this.state;
    let data = this.state;
    console.log(data);
    let modifiedData = {
      key: 'account',
      value: JSON.stringify(data),
    };
    store(modifiedData);
    let propData = {...data};
    this.props.accountDetails(propData);
    alert('saved');
  };

  onChangeName = (value) => {
    this.setState({
      name: value,
    });
  };

  onChangeAddress = (value) => {
    this.setState({
      address: value,
    });
  };

  onPressSave = () => {
    this.saveData();
  };

  onPressTime = (value) => {
    console.log(value);
    this.setState({
      time: value,
      timeModalVisible: false,
    });
  };

  onPressSelectTime = () => {
    this.setState({
      timeModalVisible: true,
    });
  };

  onPressClose = () => {
    this.setState({
      timeModalVisible: false,
    });
  };

  renderTimeModal = () => {
    return (
      <View style={styles.timeContainer}>
        <Text style={styles.modalText}>Select Draw Time</Text>
        <View style={styles.timeRowContainer}>
          <TouchableOpacity
            style={styles.timeButton}
            onPress={() => this.onPressTime('M')}>
            <Text style={[styles.modalText, {color: '#FFFFFF'}]}>M</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.timeButton}
            onPress={() => this.onPressTime('D')}>
            <Text style={[styles.modalText, {color: '#FFFFFF'}]}>D</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.timeButton}
            onPress={() => this.onPressTime('MD')}>
            <Text style={[styles.modalText, {color: '#FFFFFF'}]}>MD</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  render() {
    let {name, address, date, time, timeModalVisible} = this.state;
    return (
      <View style={styles.mainContainer}>
        <View style={styles.headerContainer}>
          <Text style={styles.headerText}>Account Info</Text>
          <View style={styles.line} />
        </View>
        <View style={styles.infoContainer}>
          <Image
            style={styles.accountIcon}
            source={require('../../assets/images/name.png')}
          />
          <TextInput
            style={styles.input}
            onChangeText={(text) => this.onChangeName(text)}
            value={name}
          />
        </View>
        <View style={styles.infoContainer}>
          <Image
            style={styles.addressIcon}
            source={require('../../assets/images/address.png')}
          />
          <TextInput
            style={styles.input}
            onChangeText={(text) => this.onChangeAddress(text)}
            value={address}
          />
        </View>

        <View style={styles.infoContainer}>
          <Image
            style={styles.clockIcon}
            source={require('../../assets/images/time.png')}
          />
          <TouchableOpacity onPress={() => this.onPressSelectTime()}>
            <Text style={styles.timeInputText}>{time}</Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          style={styles.button}
          onPress={() => this.onPressSave()}>
          <Image
            style={styles.saveIcon}
            source={require('../../assets/images/edit.png')}
          />
          <Text style={styles.saveText}>Save</Text>
        </TouchableOpacity>
        <Modal
          isVisible={timeModalVisible}
          onBackdropPress={() => this.onPressClose()}
          hideModalContentWhileAnimating={true}>
          {this.renderTimeModal()}
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  infoContainer: {
    width: DEVICE_WIDTH * 0.9,
    height: DEVICE_HEIGHT * 0.07,
    borderBottomWidth: 1,
    borderColor: '#EFE4E4',
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: DEVICE_HEIGHT * 0.05,
  },
  timeContainer: {
    height: DEVICE_HEIGHT * 0.3,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFFFFF',
    borderRadius: DEVICE_HEIGHT * 0.03,
  },
  accountIcon: {
    tintColor: '#382933',
    width: DEVICE_WIDTH * 0.05,
    height: DEVICE_HEIGHT * 0.05,
    resizeMode: 'contain',
  },
  addressIcon: {
    tintColor: '#382933',
    width: DEVICE_WIDTH * 0.055,
    height: DEVICE_HEIGHT * 0.055,
    resizeMode: 'contain',
  },
  calendarIcon: {
    tintColor: '#382933',
    width: DEVICE_WIDTH * 0.05,
    height: DEVICE_HEIGHT * 0.05,
    resizeMode: 'contain',
  },
  clockIcon: {
    tintColor: '#382933',
    width: DEVICE_WIDTH * 0.05,
    height: DEVICE_HEIGHT * 0.05,
    resizeMode: 'contain',
  },
  saveIcon: {
    tintColor: '#FFFFFF',
    width: DEVICE_WIDTH * 0.05,
    height: DEVICE_HEIGHT * 0.05,
    resizeMode: 'contain',
    marginRight: DEVICE_WIDTH * 0.02,
  },
  input: {
    flex: 1,
    fontSize: DEVICE_HEIGHT * 0.03,
    fontFamily: 'Alata-Regular',
    paddingLeft: DEVICE_WIDTH * 0.04,
  },
  timeInputText: {
    fontSize: DEVICE_HEIGHT * 0.03,
    fontFamily: 'Alata-Regular',
    paddingLeft: DEVICE_WIDTH * 0.04,
  },
  modalText: {
    fontSize: DEVICE_HEIGHT * 0.03,
    color: '#382933',
    fontFamily: 'Alata-Regular',
    marginBottom: DEVICE_HEIGHT * 0.01,
  },
  button: {
    width: DEVICE_WIDTH * 0.7,
    height: DEVICE_HEIGHT * 0.07,
    borderRadius: DEVICE_HEIGHT * 0.1,
    backgroundColor: '#382933',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  timeButton: {
    width: DEVICE_WIDTH * 0.2,
    height: DEVICE_HEIGHT * 0.07,
    borderRadius: DEVICE_HEIGHT * 0.1,
    backgroundColor: '#382933',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  headerContainer: {
    height: DEVICE_HEIGHT * 0.08,
    width: DEVICE_WIDTH * 1,
    borderBottomWidth: 1,
    borderBottomColor: '#F1F1F1',
    marginBottom: DEVICE_HEIGHT * 0.1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  headerText: {
    fontSize: DEVICE_HEIGHT * 0.035,
    color: '#382933',
    fontFamily: 'Alata-Regular',
  },
  line: {
    marginTop: DEVICE_HEIGHT * 0.01,
    borderBottomColor: '#382933',
    borderBottomWidth: 3,
    width: DEVICE_WIDTH * 0.2,
  },
  saveText: {
    fontSize: DEVICE_HEIGHT * 0.025,
    color: '#FFFFFF',
    fontFamily: 'Alata-Regular',
    fontWeight: 'bold',
  },
  timeRowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '100%',
    marginTop: DEVICE_HEIGHT * 0.04,
  },
});

const mapStateToProps = (state) => {
  return {
    account: state.accountReducer.account,
    isLogin: state.accountReducer.isLogin,
    accountDetails: state.accountReducer.accountDetails,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    login: (payload) => dispatch(login(payload)),
    accountDetails: (payload) => dispatch(accountDetails(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
