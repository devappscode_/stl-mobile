import React, {Component} from 'react';

import {StyleSheet, View, Image, Text} from 'react-native';
import {DEVICE_HEIGHT, DEVICE_WIDTH} from '../constants/constants';
import Separator from '../components/separator';

export default class HistoryItem extends Component {
  componentDidMount() {}

  toMoney = (amount) => {
    return amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  };

  renderItem = (data) => {
    let {bet_amount, dateTime} = data.item;
    let {index} = data;
    let formattedAmount = this.toMoney(bet_amount);
    return (
      <View style={styles.mainContainer} key={index}>
        <Image
          style={styles.historyIcon}
          source={require('../assets/images/history.png')}
        />
        <Text style={styles.dateTimeText}>{dateTime}</Text>
        <Separator />
      </View>
    );
  };

  renderItemList = (data) => {
    var dataArray = [];

    for (var i in data) {
      let dateTime = i;
      let bet_amount = data[i];

      dataArray.push({dateTime, bet_amount});
    }
    const list = dataArray.map((item, index) => {
      let data = {
        item,
        index,
      };
      let returnItem =
        item.bet_amount != '0' && item.bet_amount != null
          ? this.renderItem(data)
          : null;
      // return <BetItem data={data} key={index} />;
      return returnItem;
    });

    return list;
  };

  render() {
    let {data} = this.props;
    return this.renderItemList(data);
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flexDirection: 'row',
    height: DEVICE_HEIGHT * 0.07,
    width: DEVICE_WIDTH * 1,
    alignItems: 'center',
  },
  dateTimeText: {
    fontSize: DEVICE_HEIGHT * 0.027,
    color: '#0C3B2E',
    fontFamily: 'Alata-Regular',
    fontWeight: 'bold',
    marginLeft: DEVICE_WIDTH * 0.05,
  },
  historyIcon: {
    marginLeft: DEVICE_WIDTH * 0.05,
    tintColor: '#382933',
    width: DEVICE_WIDTH * 0.07,
    resizeMode: 'contain',
  },
});
