import AsyncStorage from '@react-native-community/async-storage';

export async function store(config) {
  let {key, data} = config;
  console.log('Modifying data on key ' + key);
  try {
    await AsyncStorage.setItem(key, data);
  } catch (error) {
    // Error saving data
  }
}

export async function retrieve(key, callback) {
  console.log('Retreiving data of ' + key);
  try {
    const value = await AsyncStorage.getItem(key);
    if (value !== null) {
      // We have data!!
      console.log('Successfully Retrieved Data of ' + key);
      callback(value);
    } else {
      console.log('Failed to Retrieved Data of ' + key);
      callback('{}');
    }
  } catch (error) {
    // Error retrieving data
  }
}

export async function remove(key, callback) {
  try {
    await AsyncStorage.removeItem(key);
    return true;
  } catch (exception) {
    return false;
  }
}
