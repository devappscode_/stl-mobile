//ayncstorage functions

import {store, retrieve, remove} from '../storage/index';

//initialize data from async storage
export function initData(config) {
  const {key, callback} = config;
  console.log('\n\n');
  console.log('Initializing data of ' + key);
  retrieve(key, callback);
}

export function modifyData(config) {
  store(config);
}

//reset data on async storage
export function resetData(config) {
  let resetData = {
    key: 'l2',
    value: '',
  };
  store(resetData);
}
