import React from 'react';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';

import Home from '../screens/Main/Home';
import Bet from '../screens/Main/Bet';
import Data from '../screens/Main/Data';

import Transact from '../screens/Main/Transact';
import Summary from '../screens/Main/Transact/Summary';

import Lastwo from '../screens/Main/Bet/Lastwo';
import Suertres from '../screens/Main/Bet/Suertres';

import MainTabs from '../components/MainTabs';
import BetTabs from '../components/BetTabs';

const authStack = createStackNavigator();
const mainTabStack = createBottomTabNavigator();
const appStack = createStackNavigator();
const betTabStack = createMaterialTopTabNavigator();
const transactStack = createStackNavigator();

const twoStack = createStackNavigator();
const threeStack = createStackNavigator();

const MainTabStackScreen = () => (
  <mainTabStack.Navigator tabBar={(props) => <MainTabs {...props} />}>
    <mainTabStack.Screen name="Lastwo" component={TwoStackScreen} />
    <mainTabStack.Screen name="Suertres" component={ThreeStackScreen} />
    <mainTabStack.Screen name="Data" component={Data} />
  </mainTabStack.Navigator>
);

const TwoStackScreen = () => (
  <twoStack.Navigator screenOptions={{headerShown: false}}>
    <twoStack.Screen name="Lastwo" component={Lastwo} />
    <twoStack.Screen name="Transact" component={Transact} />
    <twoStack.Screen name="Summary" component={Summary} />
  </twoStack.Navigator>
);

const ThreeStackScreen = () => (
  <threeStack.Navigator screenOptions={{headerShown: false}}>
    <twoStack.Screen name="Suertres" component={Suertres} />
    <threeStack.Screen name="Transact" component={Transact} />
    <threeStack.Screen name="Summary" component={Summary} />
  </threeStack.Navigator>
);

const BetTabStackScreen = () => (
  <betTabStack.Navigator
    swipeEnabled={false}
    tabBar={(props) => <BetTabs {...props} />}>
    <betTabStack.Screen name="Lastwo" component={Lastwo} />
    <betTabStack.Screen name="Suertres" component={Suertres} />
  </betTabStack.Navigator>
);

const TransactStackScreen = () => (
  <transactStack.Navigator screenOptions={{headerShown: false}}>
    <transactStack.Screen name="Transact" component={Transact} />
    <transactStack.Screen name="Summary" component={Summary} />
  </transactStack.Navigator>
);

const AppStackScreen = () => (
  <appStack.Navigator>
    <appStack.Screen name="Main" component={MainTabStackScreen} />
  </appStack.Navigator>
);

export default () => (
  <NavigationContainer>
    <AppStackScreen />
  </NavigationContainer>
);
